<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->foreignId('account_id')->constrained()->onDelete('cascade');
            $table->foreignId('symbol_id')->constrained()->onDelete('cascade');
            $table->string('open_date');
            $table->string('close_date')->nullable();
            $table->enum('type', ['long', 'short']);
            $table->unsignedDecimal('size', 30, 10);
            $table->unsignedDecimal('account_size', 30, 10);
            $table->decimal('profit', 20, 10)->nullable();
            $table->unsignedDecimal('price', 30, 10);
            $table->unsignedDecimal('sl', 30, 10)->nullable();
            $table->unsignedDecimal('tp', 30, 10)->nullable();
            $table->text('explanation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
