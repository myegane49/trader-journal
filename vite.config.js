import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@use "/resources/scss/_variables.scss" as *;'
      }
    }
  },

  plugins: [
    laravel({
      input: [
        'resources/pages/scss/home.scss', 'resources/js/home.js',
        'resources/pages/scss/journal.scss', 'resources/js/journal.js',
        'resources/pages/scss/profile.scss', 'resources/js/profile.js',
        'resources/pages/scss/resetPassword.scss', 'resources/js/resetPassword.js',
        'resources/pages/scss/verifyEmail.scss', 'resources/js/verifyEmail.js',
        'resources/pages/scss/forgotPassword.scss', 'resources/js/forgotPassword.js',
        'resources/pages/scss/calculator.scss', 'resources/js/calculator.js',
        'resources/pages/scss/authFirst.scss', 'resources/js/authFirst.js',
      ],
      refresh: true,
    }),
    
    vue({
      template: {
        transformAssetUrls: {
          base: null,
          includeAbsolute: false
        }
      }
    })
  ],
});
