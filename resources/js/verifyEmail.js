import './bootstrap'

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import VerifyEmail from './vue/VerifyEmail.vue'

const app = createApp()
app.component("verify-email", VerifyEmail)

app.mount("#verifyEmail")
