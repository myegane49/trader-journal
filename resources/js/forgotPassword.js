import './bootstrap'

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import ForgotPassword from './vue/ForgotPassword.vue'

const app = createApp()

app.component("forgot-password", ForgotPassword)

app.mount("#forgotPassword")
