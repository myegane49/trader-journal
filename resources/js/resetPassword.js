import './bootstrap'

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import ResetPassword from './vue/ResetPassword.vue'

const app = createApp()

app.component('reset-password', ResetPassword)

app.mount("#resetPassword")
