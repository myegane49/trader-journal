import './bootstrap';

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import Profile from './vue/Profile.vue'

const app = createApp()

app.component('app-profile', Profile)

app.mount("#profile")
