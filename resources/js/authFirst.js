import './bootstrap'

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import Auth from './vue/Home/Auth.vue'

const app = createApp()

app.component('app-auth', Auth)

app.mount("#authenticate")
