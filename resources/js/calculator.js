import './bootstrap'

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import Calculator from './vue/Calculator.vue'

const app = createApp()

app.component('app-calculator', Calculator)

app.mount("#calculator")
