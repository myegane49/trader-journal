import './bootstrap';

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import Journal from './vue/Journal/Journal.vue'

const app = createApp()

app.component('app-journal', Journal)

app.mount("#journal")
