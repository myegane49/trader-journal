import './bootstrap'

import { createApp  } from 'vue/dist/vue.esm-bundler.js'

import Auth from './vue/Home/Auth.vue'
import AuthenticatedMenu from './vue/components/AuthenticatedMenu.vue'

const app = createApp()

app.component('app-auth', Auth)
app.component('authenticated-menu', AuthenticatedMenu)

app.mount("#auth")
