<template>
  <form class="journalForm">
    <h2 class="journalForm__title">{{type == 'add' ? 'افزودن ترید' : 'تغییر ترید'}}</h2>

    <p class="journalForm__formMessage journalForm__formMessage--success">{{formSuccess}}</p>
    <p class="journalForm__formMessage journalForm__formMessage--fail">{{formFail}}</p>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="text" name="account" autocomplete="off" v-model="trade.accountTitle" required
        @input="filterAccounts" @focus="filterAccounts" @blur="blurAccount">
      <label>حساب</label>
      <ul class="loadedResult">
        <li class="loadedResult__item" v-for="account in filteredAccounts" :key="account.id" @click="selectAccount">
          {{account.title}}
        </li>
      </ul>
    </div>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="text" name="symbol" autocomplete="off" v-model="trade.symbolName" required
        @input="filterSymbols" @blur="blurSymbol" @focus="focusSymbols">
      <label>نماد</label>
      <ul class="loadedResult">
        <li class="loadedResult__item" v-for="symbol in filteredSymbols" :key="symbol.id" @click="selectSymbol">
          {{symbol.name}}
        </li>
      </ul>
    </div>

    <div class="journalForm__inputBox">
      <i class="fas fa-calendar journalForm__calendar" @click="openCalendar = true"></i>
      <input class="journalForm__input journalForm__input--date" :id="type == 'add' ? 'open-date' : 'edit-open-date'" type="text"
        v-model="trade.open_date" name="open_date" required readonly />
      <date-picker type="datetime" :element="type == 'add' ? 'open-date' : 'edit-open-date'" v-model="trade.open_date" @close="openCalendar = false"
        format="jYYYY-jMM-jDD HH:mm" :show="openCalendar" color="#40FA2F" />
      <label>تاریخ باز کردن</label>
    </div>

    <div class="journalForm__inputBox">
      <i class="fas fa-calendar journalForm__calendar" @click="closeCalendar = true"></i>
      <input class="journalForm__input journalForm__input--date" :id="type == 'add' ? 'close-date' : 'edit-close-date'" type="text"
        v-model="trade.close_date" name="close_date" readonly />
      <date-picker type="datetime" :element="type == 'add' ? 'close-date' : 'edit-close-date'" v-model="trade.close_date" @close="closeCalendar = false"
        format="jYYYY-jMM-jDD HH:mm" :show="closeCalendar" color="#40FA2F" />
      <label>تاریخ بستن</label>
    </div>
    
    <div class="journalForm__inputBox">
      <select class="journalForm__input" name="type" v-model="trade.type" required>
        <option value="long">خرید</option>
        <option value="short">فروش</option>
      </select>
      <label>نوع</label>
    </div>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="number" step="0.0000000001" name="size" v-model="trade.size" required>
      <label>حجم معامله</label>
    </div>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="number" step="0.0000000001" name="account_size" v-model="trade.account_size" required>
      <label>حجم حساب</label>
    </div>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="number" step="0.0000000001" name="profit" v-model="trade.profit">
      <label>سود (+/-)</label>
    </div>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="number" step="0.0000000001" name="price" v-model="trade.price" required>
      <label>قیمت</label>
    </div>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="number" step="0.0000000001" name="sl" v-model="trade.sl">
      <label>حد زیان</label>
    </div>

    <div class="journalForm__inputBox">
      <input class="journalForm__input" type="number" step="0.0000000001" name="tp" v-model="trade.tp">
      <label>حد سود</label>
    </div>

    <div class="journalForm__inputBox">
      <textarea class="journalForm__input" name="explanation" v-model="trade.explanation" dir="rtl"></textarea>
      <label>توضیحات</label>
    </div>

    <button class="journalForm__btn" :disabled="sending" v-if="type == 'add'" @click.prevent="addTrade">
      <span v-if="!sending">افزودن</span>
      <i v-if="sending" class="loader fas fa-spinner"></i> 
    </button>

    <button class="journalForm__btn" :disabled="sending" v-if="type == 'edit'" @click.prevent="editTrade">
      <span v-if="!sending">تغییر</span>
      <i v-if="sending" class="loader fas fa-spinner"></i> 
    </button>
  </form>
</template>

<script>
import axios from 'axios'
import DatePicker from 'vue3-persian-datetime-picker'

export default {
  data() {
    return {
      sending: false,
      formSuccess: '',
      formFail: '',
      filteredAccounts: [],
      filteredSymbols: [],
      loadedSymbols: [],
      openCalendar: false,
      closeCalendar: false
    }
  },
  components: {DatePicker},
  props: ['type', 'loadedAccounts', 'addAccountItem', 'trade', 'updateTradeItem'],
  methods: {
    async addTrade() {
      try {
        this.formSuccess = ''
        this.formFail = ''
        this.sending = true
        const res = await axios.post('/addTrade', this.trade)
        this.sending = false
        this.formSuccess = res.data.message
        this.addAccountItem(res.data.account)
        this.clearFields()
      } catch(err) {
        this.sending = false
        console.log(err)
      }
    },
    async editTrade() {
      try {
        this.formSuccess = ''
        this.formFail = ''
        this.sending = true
        const res = await axios.post(`/editTrade`, this.trade)
        this.sending = false
        this.formSuccess = res.data.message
        this.addAccountItem(res.data.account)
        this.updateTradeItem(res.data.trade)
      } catch(err) {
        this.sending = false
        console.log(err)
      }
    },
    filterAccounts(event) {
      if (!event.target.value) {
        this.filteredAccounts = [...this.loadedAccounts]
      } else {
        this.filteredAccounts = this.loadedAccounts.filter(item => {
          return item.title.includes(event.target.value)
        })
      }
    },
    selectAccount(event) {
      this.trade.accountTitle = event.target.textContent.trim()
    },
    blurAccount() {
      if (this.filteredAccounts.length != 0) {
        setTimeout(() => {
          this.filteredAccounts = []
        }, 200)
      }
    },
    async focusSymbols(event) {
      if (this.trade.accountTitle) {
        const res = await axios.get(`/getAccountSymbols/${this.trade.accountTitle}`)
        this.loadedSymbols = res.data
        this.filterSymbols(event)
      } else {
        this.loadedSymbols = []
      }
    },
    filterSymbols(event) {
      if (!event.target.value) {
        this.filteredSymbols = [...this.loadedSymbols]
      } else {
        this.filteredSymbols = this.loadedSymbols.filter(item => {
          return item.name.includes(event.target.value)
        })
      }
    },
    selectSymbol(event) {
      this.trade.symbolName = event.target.textContent.trim()
    },
    blurSymbol() {
      if (this.filteredSymbols.length != 0) {
        setTimeout(() => {
          this.filteredSymbols = []
        }, 200)
      }
    },
    clearFields() {
      for (let field in this.trade) {
        this.trade[field] = ''
      }
    }
  }
}
</script>

<style lang="scss">
@import './journal-form';
</style>