<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ژورنال</title>
    <link rel="stylesheet" href="{{asset('css/all.css')}}">
    @vite('resources/scss/pages/journal.scss')
</head>
<body>
    <div id="journal">
        <app-journal username="{{Auth::user()->username}}" />
    </div>
    
    @vite('resources/js/journal.js')
</body>
</html>