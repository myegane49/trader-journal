<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>تایید ایمیل</title>
  <link rel="stylesheet" href="{{asset('css/all.css')}}">
  @vite('resources/scss/pages/verifyEmail.scss')
</head>
<body>
  <div id="verifyEmail"><verify-email /></div>

  @vite('resources/js/verifyEmail.js')
</body>
</html>
