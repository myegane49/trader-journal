<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>فراموشی گذرواژه</title>
  <link rel="stylesheet" href="{{asset('css/all.css')}}">
  @vite('resources/scss/pages/forgotPassword.scss')
</head>
<body>
  <div id="forgotPassword"><forgot-password /></div>

  @vite('resources/js/forgotPassword.js')
</body>
</html>
