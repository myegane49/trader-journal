<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>باز کردن حساب کاربری</title>
  <link rel="stylesheet" href="{{asset('css/all.css')}}">
  @vite('resources/scss/pages/authFirst.scss')
</head>
<body>
  <div id="authenticate" class="atf">
    <app-auth composition="alone" />
  </div>

  @vite('resources/js/authFirst.js')
</body>
</html>