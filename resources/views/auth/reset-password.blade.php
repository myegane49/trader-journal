<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>تغییر رمز</title>
  <link rel="stylesheet" href="{{asset('css/all.css')}}">
  @vite('resources/scss/pages/resetPassword.scss')
</head>
<body>
  <div id="resetPassword">
    <reset-password token="{{$token}}"></reset-password>
  </div>

  @vite('resources/js/resetPassword.js')
</body>
</html>