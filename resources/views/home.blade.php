<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ژورنال معامله گر</title>
    <link rel="stylesheet" href="{{asset('css/all.css')}}">
    @vite('resources/scss/pages/home.scss')
</head>
<body>
    <div class="page">
        <div class="container">
            <header class="about">
                <div class="title">
                    <h1>ژورنال معامله گر</h1>
                    <img src="{{asset('images/logo.png')}}" alt="لوگو">
                </div>
                <h2 class="subHead">برای معامله گری حرفه ای در بازارهای مالی به مدیریت ریسک نیاز داری و برای مدیریت ریسک به ژورنال نویسی پس با ما همراه باش</h2>
                
            </header>
     
            <section class="interact">
                <div class="auth" id="auth">
                    @if (Auth::check())
                        <authenticated-menu username="{{Auth::user()->username}}" route="home" />
                    @else
                        <app-auth />
                    @endif
                </div>
            
                <div class="subscription">
                    <h2>طرح های عضویت</h2>
                    @if (Session::has('message'))
                        <p class="subscriptionMessage">{{Session::get('message')}}</p>
                    @endif
                    @if (!Auth::check())
                        <p dir="rtl">15 روز حساب آزمایشی</p>
                    @endif
                    @if ($message)
                        <p dir="rtl" class="subscriptionMessage">{{$message}}</p>
                    @endif
                    <p>در صورت داشتن عضویت و خرید مجدد، به مدت زمان عضویت شما افزوده خواهد شد</p>
                    <div class="subscription__plans">
                        @foreach ($plans as $plan)
                            <div class="plan">
                                <h3>{{$plan->name}}</h3>
                                <p>{{$plan->price}} تومان</p>                              
                                <form class="plan__form" action="{{route('buy.sub')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="planId" value="{{$plan->id}}">
                                    <button class="plan__btn">خرید</button>
                                </form>
                            </div>                        
                        @endforeach
                    </div>
                </div>
            </section>

            <section class="moreInfo">
                <h2 class="subHead">اگه وقت آماده کردن یک فایل اکسل رو نداری یا بلد نیستی یا اکسل نمیتونه اطلاعات دقیق بهت بده، اگه گزارش معاملاتت رو روی کاغذ می نویسی و محاسبات وقت زیادی ازت میگیره، الان وقتشه که ژورنال نویسی رو اینجا تجربه کنی</h2>
                <img src="{{asset('images/sample.png')}}" class="sample">
            </section>
        </div>    
        @include('footer')
    </div>

    @vite('resources/js/home.js')
</body>
</html>
