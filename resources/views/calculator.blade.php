<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>محاسبه اندازه پوزیشن</title>
    <link rel="stylesheet" href="{{asset('css/all.css')}}">
    @vite('resources/scss/pages/calculator.scss')
</head>
<body>
    <div id="calculator">
        <app-calculator username="{{Auth::user()->username}}" />
    </div>
    
    @vite('resources/js/calculator.js')
</body>
</html>
