<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>پروفایل</title>
    <link rel="stylesheet" href="{{asset('css/all.css')}}">
    @vite('resources/scss/pages/profile.scss')
</head>
<body>
    <div id="profile">
        <app-profile :user="{{$user}}" />
    </div>
    
    @vite('resources/js/profile.js')
</body>
</html>