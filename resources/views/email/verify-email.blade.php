<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <style>
    * {
      padding: 0;
      margin: 0;
      border: none;
      box-sizing: border-box;
    }

    body {
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      background-color: #30BA23;
      height: 100vh;
    }

    h1 {
      color: #DAD9DB;
      margin-bottom: 3rem;
    }

    a {
      border-radius: 10px;
      background-color: #DAD9DB;
      padding: 15px 30px;
      text-decoration: none;
      color: #30BA23;
    }
  </style>
</head>
<body>
  <h1>برای تایید ایمیل خود روی لینک زیر کلیک کنید</h1>

  <a href="{{$url}}">تایید ایمیل</a>
</body>
</html>