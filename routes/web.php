<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\TradeController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\SymbolController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PageController::class, 'home'])->name('home');
Route::get('/journal', [PageController::class, 'journal'])->middleware(['auth', 'verified'])->name('journal');
Route::get('/verifyEmail', [PageController::class, 'verifyEmail'])->middleware('auth')->name('verification.notice');
Route::get('/forgotPassword', [PageController::class, 'forgotPassword'])->middleware('guest');
Route::get('/reset-password/{token}', [PageController::class, 'resetPassword'])->middleware('guest')->name('password.reset');
Route::get('/profile', [PageController::class, 'profile'])->middleware(['auth'])->name('profile');
Route::get('/authFirst', [PageController::class, 'authFirst'])->name('auth.first');
Route::get('/calculator', [PageController::class, 'calculator'])->middleware(['auth', 'verified'])->name('calculator');

Route::post('/signup', [UserController::class, 'signup']);
Route::post('/signin', [UserController::class, 'signin']);
Route::get('/signout', [UserController::class, 'signout'])->name('signout');
Route::get('/emailVerification/{id}/{hash}', [UserController::class, 'emailVerification'])
    ->middleware(['auth', 'signed'])->name('verification.verify');
Route::post('/resendVerificationEmail', [UserController::class, 'resendVerificationEmail'])
    ->middleware(['auth', 'throttle:6,1'])->name('verification.send');  
Route::post('/forgotPasswordForm', [UserController::class, 'forgotPasswordForm'])->middleware('guest')->name('password.email');
Route::post('/resetPasswordForm', [UserController::class, 'resetPasswordForm'])->middleware('guest');
Route::post('/buySubscription', [UserController::class, 'buySub'])->middleware(['auth', 'verified'])->name('buy.sub');
Route::get('/addSubscription', [UserController::class, 'addSub'])->middleware(['auth', 'verified'])->name('add.sub');
Route::middleware(['auth'])->group(function() {
    Route::patch('/editUser', [UserController::class, 'update']);
      Route::delete('/deleteUser', [UserController::class, 'delete']);
    
});

Route::middleware(['auth', 'verified', 'can:isStillSubscribed,App\Models\User'])->group(function() {
    Route::post('/addTrade', [TradeController::class, 'create']);
      Route::post('/getReport', [TradeController::class, 'index']);
      Route::delete('/deleteTrade/{id}', [TradeController::class, 'delete']);
        Route::post('/editTrade', [TradeController::class, 'update']);
      
});

Route::middleware(['auth', 'verified', 'can:isStillSubscribed,App\Models\User'])->group(function() {
    Route::get('/getUserAccounts', [AccountController::class, 'index']);
      Route::post('/editAccount/{account_id}', [AccountController::class, 'update']);
      Route::delete('/deleteAccount/{account_id}', [AccountController::class, 'delete']);
      
});

Route::middleware(['auth', 'verified', 'can:isStillSubscribed,App\Models\User'])->group(function() {
    Route::get('/getAccountSymbols/{account_title}', [SymbolController::class, 'index']);
      Route::post('/editSymbol/{symbol_id}', [SymbolController::class, 'update']);
      Route::delete('/deleteSymbol/{symbol_id}', [SymbolController::class, 'delete']);
      
});

// Route::get('/test', function() {
// //   return view('auth/reset-password', ['token' => 'some-token']);
// // });
//
// })
