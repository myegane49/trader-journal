<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Symbol;
use App\Models\Account;
use App\Models\User;

class Trade extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'account_id', 'symbol_id', 'open_date', 'close_date', 'open_time', 'close_time', 'type', 'size', 'account_size', 'profit', 'price', 'sl', 'tp', 'explanation'];

    public function account() {
        return $this->belongsTo(Account::class);
    }

    public function symbol() {
        return $this->belongsTo(Symbol::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getAccountSizeAttribute($value) {
        return $value ? floatval($value) : $value;
    }
    public function getSizeAttribute($value) {
        return $value ? floatval($value) : $value;
    }
    public function getPriceAttribute($value) {
        return $value ? floatval($value) : $value;
    }
    public function getSlAttribute($value) {
        return $value ? floatval($value) : $value;
    }
    public function getTpAttribute($value) {
        return $value ? floatval($value) : $value;
    }
    public function getProfitAttribute($value) {
        return $value ? floatval($value) : $value;
    }
}
