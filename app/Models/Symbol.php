<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Account;
use App\Models\Trade;
use App\Models\User;

class Symbol extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'account_id', 'name'];

    public function account() {
        return $this->belongsTo(Account::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function trades() {
        return $this->hasMany(Trade::class);
    }
}
