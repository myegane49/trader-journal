<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Trade;
use App\Models\User;
use App\Models\Symbol;

class Account extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'title'];

    public function trades() {
        return $this->hasMany(Trade::class);
    }

    public function symbols() {
        return $this->hasMany(Symbol::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
