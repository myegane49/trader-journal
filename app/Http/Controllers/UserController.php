<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Plan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

class UserController extends Controller
{
    public function signup(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => ['required', 'max:25'],
            'email' => ['unique:users', 'required', 'email'],
            'password' => ['required', 'min:8']
        ], 
        ['email.unique' => 'کاربر با این ایمیل قبلا ثبت شده است']
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first('email');
            return response()->json(['email' => $error], 400);
        } else {
            $inputs = $request->all();
            $inputs['password'] = Hash::make($inputs['password']);

            $interval = \DateInterval::createFromDateString('15 day');
            $now = new \DateTime();
            $inputs['subscribed_until'] = $now->add($interval);
            
            if ($user = User::create($inputs)) {   
                if (Auth::attempt(['email' => $inputs['email'], 'password' => $request->password], true)) {
                    $request->session()->regenerate();
                }
                event(new Registered($user));
            }
        }
    }

    public function signin(Request $request) {
        $credentials = $request->only('email', 'password');
        $remember = $request->get('remember');

        if (Auth::attempt($credentials, $remember)) {
            $request->session()->regenerate();
        } else {
            return response()->json(['message' => 'ایمیل یا پسورد شما درست نمی باشد'], 401);
        }
    }

    public function signout(Request $request) {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('home');
    }

    public function emailVerification(EmailVerificationRequest $request) {
        $request->fulfill();
    
        return redirect()->route('home');
    }

    public function resendVerificationEmail(Request $request) {
        $request->user()->sendEmailVerificationNotification();
    
        return response()->json(['message' => 'ایمیل تایید ارسال شد!']);
    }

    public function forgotPasswordForm(Request $request) {
        $request->validate(['email' => ['required', 'email']]);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? response()->json(['status' => 'لینک تغییر گذرواژه فرستاده شد'], 200)
            : response()->json(['email' => 'خطایی در فرستادن ایمیل رخ داد'], 500);
    }

    public function resetPasswordForm(Request $request) {
        $request->validate([
            'token' => 'required',
            'email' => ['required', 'email'],
            'password' => ['required', 'min:8', 'confirmed'],
        ]);
    
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->save();
    
                $user->setRememberToken(Str::random(60));
    
                event(new PasswordReset($user));
            }
        );
    
        return $status == Password::PASSWORD_RESET
            ? response()->json(['status' => 'گذرواژه شما تغییر کرد'], 200)
            : response()->json(['email' => 'خطایی در تغییر گذرواژه رخ داد'], 500);

    }

    public function buySub(Request $request) {
        $user = $request->user();
        $plan = Plan::find($request->planId);
        $price = $plan->price * 10;
        $description = "خرید عضویت" . $plan->name;

        $client = new Client();
        $res = $client->request('POST', 'https://api.zarinpal.com/pg/v4/payment/request.json', [
            'form_params' => [
                'merchant_id' => env('MERCHANT_ID'),
                'amount' => $price,
                'description' => $description,
                'callback_url' => env('APP_URL') . '/addSubscription'
            ]
        ]);

        $user->buying_plan_id = $request->planId;
        $user->save();

        $authority = json_decode($res->getBody())->data->authority;
        return redirect("https://www.zarinpal.com/pg/StartPay/".$authority);
    }

    public function addSub(Request $request) {
        $user = $request->user();
        $status = $request->query('Status');
        $authority = $request->query('Authority');

        if ($status == 'OK') {
            $plan = Plan::find($user->buying_plan_id);
            $price = $plan->price * 10;

            $client = new Client();
            $res = $client->request('POST', 'https://api.zarinpal.com/pg/v4/payment/verify.json', [
                'form_params' => [
                    'merchant_id' => env('MERCHANT_ID'),
                    'amount' => $price,
                    'authority' => $authority
                ]
            ]);

            $res = json_decode($res->getBody());
            $code = $res->data->code;
            if ($code == 100) {
                $ref_id = $res->data->ref_id;

                $now = new \DateTime();
                $interval = \DateInterval::createFromDateString($plan->days .' day');
                $subscribedUntil = new \DateTime($user->subscribed_until);
        
                if ($subscribedUntil < $now) {
                    $user->subscribed_until = $now->add($interval);
                } else {
                    $user->subscribed_until = $subscribedUntil->add($interval);
                }
        
                $user->save();
                Session::flash('message', 'پرداخت موفق با شماره تراکنش ' . $ref_id . 'عضویت ' . $plan->name . ' برای شما ثبت شد');
                return redirect()->route('home');
            } else {
                Session::flash('message', 'پرداخت شما ناموفق بود');
                return redirect()->route('home');
            }
        } else {
            return redirect()->route('home');
        }
    }

    public function update(Request $request) {
        $user = $request->user();
        $inputs = ['username' => $request->username];
        $validations = [
            'username' => ['required', 'max:25']
        ];
        if ($request->email != $user->email) {
            $inputs['email'] = $request->email;
            $inputs['email_verified_at'] = null;
            $validations['email'] = ['unique:users', 'required', 'email'];
        }
        if ($request->password) {
            $inputs['password'] = $request->password;
            $validations['password'] = ['required', 'min:8'];
        }
    
        $validator = Validator::make($inputs, $validations, 
            ['email.unique' => 'کاربر با این ایمیل قبلا ثبت شده است']
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first('email');
            return response()->json(['email' => $error], 400);
        } else {
            if ($request->password) {
                $inputs['password'] = Hash::make($inputs['password']);
            }
            if ($user->update($inputs)) {
                return response()->json(['message' => 'حساب شما بروز شد', 'user' => $user], 200);
            } else {
                return response()->json(['message' => 'بروزرسانی با خطا مواجه شد'], 400);
            } 
        }
    }

    public function delete(Request $request) {
        if (!$request->user()->delete()) {
            return response()->json(['message' => 'درخواست شما با خطا مواجه شد'], 400);
        }
    }
}
