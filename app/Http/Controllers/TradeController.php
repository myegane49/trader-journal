<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Symbol;
use App\Models\Trade;

class TradeController extends Controller
{
    public function create(Request $request) {
        $inputs = $request->all();
        $inputs['user_id'] = $request->user()->id;

        $account = Account::firstOrCreate([
            'user_id' => $inputs['user_id'],
            'title' => $inputs['accountTitle']
        ]);
        $inputs['account_id'] = $account->id;

        $symbol = Symbol::firstOrCreate([
            'user_id' => $inputs['user_id'],
            'account_id' => $account->id,
            'name' => $inputs['symbolName']
        ]);
        $inputs['symbol_id'] = $symbol->id;

        if (Trade::create($inputs)) {
            return response()->json(['message' => 'ترید با موفقیت افزوده شد', 'account' => $account], 201);
        } else {
            return response('خطایی در سرور رخ داد', 500);
        }
    }

    public function index(Request $request) {
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $account = $request->user()->accounts()->find($request->account_id);        
        $closedTrades = $account->trades()->where('close_date', '>=', $start_date)->where('close_date', '<', $end_date)->get();
        foreach ($closedTrades as $trade) {
            $trade->symbolName = $trade->symbol->name;
            $trade->accountTitle = $account->title;
        }
        $openTrades = $account->trades()->where('close_date', null)->get();
        foreach ($openTrades as $trade) {
            $trade->symbolName = $trade->symbol->name;
            $trade->accountTitle = $account->title;
        }

        if (count($closedTrades) == 0 && count($openTrades) == 0) {
            return response()->json(['errMessage' => 'تریدی در این بازه تاریخی پیدا نشد']);
        } else {
            return response()->json(['openTrades' => $openTrades, 'closedTrades' => $closedTrades]);
        }
    }

    public function delete(Request $request, $id) {
        $request->user()->trades()->find($id)->delete();
    }

    public function update(Request $request) {
        $inputs = $request->all();
        $inputs['user_id'] = $request->user()->id;

        $account = Account::firstOrCreate([
            'user_id' => $inputs['user_id'],
            'title' => $inputs['accountTitle']
        ]);
        $inputs['account_id'] = $account->id;

        $symbol = Symbol::firstOrCreate([
            'user_id' => $inputs['user_id'],
            'account_id' => $account->id,
            'name' => $inputs['symbolName']
        ]);
        $inputs['symbol_id'] = $symbol->id;

        $trade = $request->user()->trades()->find($inputs['id']);
        if ($trade->update($inputs)) {
            $trade->symbolName = $symbol->name;
            $trade->accountTitle = $account->title;
            return response()->json([
                'message' => 'ترید با موفقیت تغییر داده شد',
                'account' => $account,
                'trade' => $trade
            ], 201);
        } else {
            return response('خطایی در سرور رخ داد', 500);
        }
    }
}
