<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index(Request $request) {
        return $request->user()->accounts()->get();
    }

    public function update(Request $request, $account_id) {
        $request->user()->accounts()->find($account_id)->update(['title' => $request->new_title]);
        return response('نام حساب با موفقیت تغییر کرد', 200);
    }

    public function delete(Request $request, $account_id) {
        $request->user()->accounts()->find($account_id)->delete();
        return response('حساب با موفقیت حذف شد', 200);
    }
}
