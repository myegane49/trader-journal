<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SymbolController extends Controller
{
    public function index(Request $request, $account_title) {
        if ($account = $request->user()->accounts()->where('title', $account_title)->first()) {
            return $account->symbols()->get();
        } else {
            return [];
        }
    }

    public function update(Request $request, $symbol_id) {
        $request->user()->symbols()->find($symbol_id)->update(['name' => $request->new_name]);
        return response('نام نماد با موفقیت تغییر کرد', 200);
    }

    public function delete(Request $request, $symbol_id) {
        $request->user()->symbols()->find($symbol_id)->delete();
        return response('نماد با موفقیت حذف شد', 200);
    }
}
