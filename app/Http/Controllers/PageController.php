<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plan;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    public function home(Request $request) {
        $plans = Plan::all();
        $user = $request->user();
        $message = '';
        if ($user) {
            $createdAt = new \DateTime($user->created_at);
            $subscribedUntil = new \DateTime($user->subscribed_until);
            $now = new \DateTime('now');
            $dteDiff  = $createdAt->diff($subscribedUntil)->days;
            $daysLeft = $now->diff($subscribedUntil)->days + 1;
            if ($dteDiff == 15) {
                $message = "حساب شما به مدت {$daysLeft} روز دیگر آزمایشی است";
            } else if ($user->cannot('isStillSubscribed', $user)) {
                $message = "عضویت شما به پایان رسیده است";
            } else {
                $message = "{$daysLeft} روز مانده تا پایان عضویت شما";
            }
        }
        return view('home', ['plans' => $plans, 'message' => $message]);
    }

    public function journal(Request $request) {
        $user = $request->user();
        if ($user->cannot('isStillSubscribed', $user)) {
            return back();
        } else {
            return view('journal', ['user' => $user]);
        }
    }

    public function verifyEmail() {
        return view('auth/verify-email');
    }

    public function forgotPassword() {
        return view('auth/forgot-password');
    }

    public function resetPassword($token) {
        return view('auth/reset-password', ['token' => $token]);
    }

    public function profile(Request $request) {
        return view('profile', ['user' => $request->user()]);
    }

    public function authFirst() {
        return view('auth/auth-first');
    }

    public function calculator(Request $request) {
        $user = $request->user();
        if ($user->cannot('isStillSubscribed', $user)) {
            return back();
        } else {
            return view('calculator', ['user' => $user]);
        }
    }

}
